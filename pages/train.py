"""
This module contains TrainPage,
the page object for Main/Train page.
"""

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from datetime import datetime, timedelta
import time
import re
import logging

logging.basicConfig(level=logging.INFO)

class TrainPage:

    URL = 'https://proizd.ua/en'

    def __init__(self, browser):
        self.browser = browser

    def load(self):
        self.browser.get(self.URL)
    
    def get_main_search_title(self):
        '''Retrieving the title in the main search container'''
        return self.browser.find_element_by_tag_name('h1').text
    
    def count_logos(self):
        '''Count the number of logo images on the main/train page'''
        return len(self.browser.find_elements_by_xpath("//img[contains(@alt, 'proizd-logo')]"))
    
    def count_main_sections(self):
        '''Count the number of main sections on the main/train page'''
        return len(self.browser.find_elements_by_xpath("//div[contains(@class, 'visible-space')]"))
    
    def route(self, from_, to_, day):
        '''Searching for the train route'''
        departure = self.browser.find_element_by_id('departure-input')
        departure.clear()
        departure.send_keys(from_)
        time.sleep(2)
        departure_pick = self.browser.find_element_by_xpath("//*[contains(@class, 'station-item')]")
        departure_pick.click()

        arrival = self.browser.find_element_by_id('arrival-input')
        arrival.clear()
        arrival.send_keys(to_)
        time.sleep(2)
        arrival_pick = self.browser.find_element_by_xpath("//*[contains(@class, 'station-item')]")
        arrival_pick.click()

        self.browser.find_element_by_tag_name("date-picker").click()
        time.sleep(1)
        self.browser.find_element_by_xpath(f"//span[contains(text(), '{day}')]").click()

        button = self.browser.find_element_by_id('main-search-btn')
        button.click()
        time.sleep(4)
    
    def trips(self):
        '''Retrieving the train route search results as a list of dictionaries'''
        trips = []
        trip_divs = self.browser.find_elements_by_xpath("//div[contains(@class, 'trip-card')]")
        for td in trip_divs:
            train_number = td.find_element_by_xpath(".//span[contains(@class, 'transport-name-number')]").text
            logging.info(f'Train number: {train_number}')
            departure_station = td.find_element_by_xpath(".//*[contains(@class, 'departure-station-name')]").text
            logging.info(f'Departure station: {departure_station}')
            departure_datetime_string = td.find_elements_by_xpath(".//p[contains(@class, 'time-value')]")[0].text \
                                        + ' ' + td.find_elements_by_xpath(".//div[contains(@class, 'date-value')]")[0].text \
                                        + ' ' + str(datetime.now().year)
            logging.info(f'Departure datetime string: {departure_datetime_string}')
            departure_datetime = datetime.strptime(departure_datetime_string, '%H:%M %B %d, %a %Y')
            arrival_station = td.find_element_by_xpath(".//*[contains(@class, 'arrival-station-name')]").text
            arrival_datetime_string = td.find_elements_by_xpath(".//p[contains(@class, 'time-value')]")[1].text \
                                        + ' ' + td.find_elements_by_xpath(".//div[contains(@class, 'date-value')]")[1].text \
                                        + ' ' + str(datetime.now().year)
            arrival_datetime = datetime.strptime(arrival_datetime_string, '%H:%M %B %d, %a %Y')
            data = {
                'train_number': train_number,
                'departure_station': departure_station,
                'departure time': departure_datetime,
                'arrival_station': arrival_station,
                'arrival time': arrival_datetime,
                'travel time': arrival_datetime - departure_datetime
            }
            trips.append(data)
            
        return trips

    def trips_number(self):
        '''Counting the train route search results'''
        result = self.browser.find_element_by_xpath("//*[contains(@class, 'trips-number')]").text
        logging.info(f'Result: {result}')
        pattern = re.compile(r'Result[s]*: ([0-9]*)', re.I)
        match = pattern.search(result)
        return int(match.group(1))
    
    def sort_by(self, by):
        '''Sorting the train route search results '''
        btn = self.browser.find_element_by_xpath("//button[contains(@class, 'trips-sorting-btn')]")
        btn.click()
        sort_by = self.browser.find_element_by_xpath(f"//li[contains(text(), '{by}')]")
        sort_by.click()
        time.sleep(1)