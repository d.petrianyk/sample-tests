import pytest
import requests
import json
import time


JSON = '''{
    "language": "en",
    "supplier": "uz_train",
    "requestId": "0188223a68c9",
    "transactionId": "4c84de3c0120",
    "sessionId": "c7a3b4b3cb40",
    "userId": "9efb0673df64",
    "arrivalCode": "2200001",
    "departureCode": "2204580",
    "departureDate": "2020-07-24"
    }'''

BASE_URL = 'https://de-prod-lb.cashalot.in.ua'



@pytest.mark.parametrize('language,requestId,transactionId,sessionId,userId,arrivalCode,departureCode,departureDate,error_code,status_code', 
                            [
                                ('en', "0188223a68c9", "4c84de3c0120", "c7a3b4b3cb40", "9efb0673df64", "2200001", "2204580", "2020-07-24",0,200),
                                ('e', "0188223a68c9", "4c84de3c0120", "c7a3b4b3cb40", "9efb0673df64", "2200001", "2204580", "2020-07-24",20,200),
                                ('en', "0188223a68c9", "4c820", "c7a3b4b3cb40", "9efb0673df64", "2200001", "2204580", "2020-07-24",0,500),
                                ('en', "0188223a68c9", "4c84de3c0120", "c7ab40000000000", "9efb0673df64", "2200001", "2204580", "2020-07-24",0,500),
                                ('en', "0188223a68c9", "4c84de3c0120", "c7a3b4b3cb40", "9efb0673df64", "2200001", "2204580", "2020-07-14",700,200),
                            ]
                        )
def test_search_with_valid_invalid_data(language, requestId, transactionId, sessionId, userId, arrivalCode, departureCode, departureDate, error_code, status_code):
    # SetUp
    url = BASE_URL + '/cashalot_main/rest/supplier/search'
    headers = {'content-type': 'application/json'}
    payload = {
        "language": language,
        "supplier": "uz_train",
        "requestId": requestId,
        "transactionId": transactionId,
        "sessionId": sessionId,
        "userId": userId,
        "arrivalCode": arrivalCode,
        "departureCode": departureCode,
        "departureDate": departureDate
    }

    # Exercise
    start = time.time()
    resp = requests.post(url, headers=headers, data=json.dumps(payload))
    duration = time.time() - start
    resp_body = resp.json()

    # Verify
    assert resp.status_code == status_code
    assert int(resp_body.get('errorCode', 0)) == error_code
    assert duration <= 200


def test_train_search():
    # SetUp
    url = BASE_URL + '/cashalot_main/rest/supplier/search'
    headers = {'content-type': 'application/json'}
    payload = json.loads(JSON)

    # Exercise
    start = time.time()
    resp = requests.post(url, headers=headers, data=json.dumps(payload))
    duration = time.time() - start
    resp_body = resp.json()

    # Verify
    assert resp.status_code == 200
    assert resp_body['departureStation']['code'] == payload['departureCode']
    assert resp_body['arrivalStation']['code'] == payload['arrivalCode']
    assert resp_body['departureDate'] == payload['departureDate']
    assert duration <= 200  